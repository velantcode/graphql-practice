import mongoose from "mongoose";

async function connect() {
  try {
    console.log(">>> Connecting with DB ...");

    await mongoose.connect("mongodb://localhost/graphqldb", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    });

    console.log(">>> DB is connected!!");
  } catch (e) {
    console.log(">>> Something wrong while database connection :( !!");
    console.log("Error: ", e);
  }
}

export default connect;
