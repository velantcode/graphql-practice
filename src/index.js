import express from "express";
import graphqlHTTP from "express-graphql";
import schema from "./GraphqlSettings/scheme";
import connect from "./database";

const app = express();

// init db
connect();

app.get("/", (req, res) => {
  res.json({
    message: "Hello world"
  });
});

app.use(
  "/graphql",
  graphqlHTTP({
    graphiql: true,
    schema,
    context: {
      messageId: "test"
    }
  })
);

app.listen(3000, () => console.log("Server on port: 3000"));
