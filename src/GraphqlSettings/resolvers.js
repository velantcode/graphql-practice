import Users from "../Models/Users";
import Task from "../Models/Tasks";

const resolvers = {
  Query: {
    hello() {
      return "Hello World with GraphQL";
    },
    greet(_, { name }, ctx) {
      console.log(ctx);
      return `Hello ${name}!`;
    },
    tasks() {
      return Task;
    },
    async users() {
      return Users.find().exec();
    }
  },
  Mutation: {
    createTask(_, { input }) {
      input._id = Task.length;
      Task.push(input);
      return input;
    },
    async createUser(_, { input }) {
      const newUser = new Users(input);
      await newUser.save();
      return newUser;
    },
    async updateUser(_, { _id, input }) {
      const user = await Users.findByIdAndUpdate(_id, input, {
        new: true
      }).exec();

      if (user) {
        // updated user data
        return user;
      }

      console.log("*** Mutation -> updateUser() : Not found user ***");
      return null;
    },
    async deleteUser(_, { _id }) {
      const user = await Users.findByIdAndDelete(_id).exec();

      if (user) {
        // delete user
        await user.delete();

        return user;
      }

      console.log("*** Mutation -> deleteUser() : Not found user ***");
      return null;
    }
  }
};

export default resolvers;
