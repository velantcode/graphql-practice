import { Schema, model } from "mongoose";

const userSchema = new Schema({
  firstName: { type: String, require: true },
  lastName: { type: String },
  age: { type: Number }
});

module.exports = model("users", userSchema);
