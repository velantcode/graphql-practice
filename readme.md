# PitbullPay API

[![pipeline status](https://gitlab.com/velantcode/graphql-practice/badges/master/pipeline.svg)](https://gitlab.com/velantcode/graphql-practice/commits/master)

## Install

* Clone this repository: `git clone git@gitlab.com:velantcode/graphql-practice.git`

* IMPORTANT!!! You must have MongoDB installed.

* Build Setup

      ## bash
      # Install dependencies
      $ npm install
      
      # Serve with hot reload (develop)
      $ npm run dev
      
      # Serve without hot reload
      $ npm run start
      
      # Build for release and run server
      $ npm run serve
      
      # Confirms that the server works
      http://localhost:3000/

## Additionals Commands

The nexts commands can use 

    ## bash

    # Compile the project
    $ npm run build

    # Delete the `dist` folder
    $ npm run clear

## More documentation

### Express
Official documentation [Express.js Docs](https://expressjs.com).

### GraphQL
Official documentation [Graphql Docs](https://graphql.org/).